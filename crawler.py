from urllib.request import urlopen
from bs4 import BeautifulSoup as soup
from multiprocessing.dummy import Pool as ThreadPool
import re
import pdb

BASE_URL = "https://www.epocacosmeticos.com.br/"

class Crawler:
    def __init__(self, base_url, filename='products.csv'):
        if not base_url:
            raise Exception("You need to pass the start url")
        if not isinstance(base_url, str):
            raise Exception("The start url has to be a string")
        self.base_url = base_url
        self.checked_urls = set()
        self.content  = set()
        self.unchecked_urls = [self.base_url]
        self.filename = filename

    def parse_page(self, url):
        # pdb.set_trace()
        try:
            client = urlopen(url)
            page_html = client.read()
            page_soup = soup(page_html, "html.parser")
            if url.endswith('/p'):
                self.extract_content(url, page_soup)
            else:
                self.extract_links(page_soup)
        except Exception:
            print("connection error")

    def extract_content(self, url, page):
        # pdb.set_trace()
        try:
            self.content = url, page.title.text, page.find(
                'div', class_="productName").text
            return self.content
        except AttributeError:
            print("Attribute error")

    def extract_links(self, page):
        # pdb.set_trace()
        for link in page.findAll('a', attrs={'href': re.compile(BASE_URL)}):
            self.unchecked_urls.append(link.get('href'))
        return self.unchecked_urls

    def run(self):
        unchecked_links = self.unchecked_urls
        while unchecked_links:
            for url in unchecked_links:
                if not (url not in self.checked_urls):
                    break
                self.parse_page(url)
                self.checked_urls.add(url)
            unchecked_links = set(self.unchecked_urls) - self.checked_urls
            print(self.content)

if __name__ == '__main__':
    pool = ThreadPool(20)
    c = Crawler(BASE_URL)
    # pdb.set_trace()
    content = (c.run(), c.content)
    pool.close()
    pool.join()
